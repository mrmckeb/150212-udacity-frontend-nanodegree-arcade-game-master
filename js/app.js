"use strict";



/*  Grid setup -----

    The grid is made up of tiles, 101x83 each. The below
    variables are used in calculations throughout the game
    to ensure that movement and placement of the games
    elements is tile-based.

    The imageOffset variable adjusts the position of the
    bugs and player so that they align with the tiles
    they're standing on.

*/
var grid = {
    tileWidth: 101,
    tileHeight: 83,
    width: 505,
    height: 498,
    imageOffset: 24
}



/*  Sprite Superclass -----

    Takes a sprite, along with initial x and y coordinates,
    and optional width and height values (for hitboxes) as
    initial inputs when constucting.

*/

//  Creates a new Sprite
var Sprite = function(sprite, x, y, w, h) {

    // Sets the sprite image
    this.sprite = sprite;

    // Sets the sprite coordinates
    this.x = x;
    this.y = y - grid.imageOffset; // Adjusted with offset

    // Optional for sprites with hitboxes
    this.width = w;
    this.height = h;
};

//  Draws our Sprites on screen
Sprite.prototype.render = function() {
    ctx.drawImage(Resources.get(this.sprite), this.x, this.y);
};



/*  Collectibles Subclass -----

    Creates a collectible element on-screen, as an instance
    of the Sprite superclass. This element has a hitbox,
    but doesn't have any movement/animation properties.

*/

//  Creates a new Collectible from Sprite
var Collectible = function(sprite, x, y, w, h) {
    Sprite.call(this, sprite, x, y, w, h);
};

//  Ensure each Collectible has access to Sprite's prototypes
Collectible.prototype = Object.create(Sprite.prototype);

//  Sets the constructor of Collectible back to Collectible
Collectible.prototype.constructor = Collectible;



/*  Enemy Subclass -----

    The Enemy subclass is an instance of the Sprite
    superclass, taking in a position and speed.

*/

//  Creates a new Enemy from Sprite
var Enemy = function(x, y, speed) {
    Sprite.call(this, 'images/enemy-bug.png', x, y, 96, 66);
    this.speed = speed;
};

//  Ensure each Enemy has access to Sprite's prototypes
Enemy.prototype = Object.create(Sprite.prototype);

//  Sets the constructor of Enemy back to Enemy
Enemy.prototype.constructor = Enemy;

/*  Updates the Enemy's position, with dt (delta time)
    used to ensure speeds are calculated the same way
    on all systems - regardless of their speeds.
    http://en.wikipedia.org/wiki/Delta_timing */
Enemy.prototype.update = function(dt) {

    /* If the element is inside the grid, continue moving,
    otherwise return to the start of that row. */
    if (this.x < grid.width) {
        // Multiply speed by time delta and add difficulty
        this.x += this.speed * (dt * 100) + diffAdditive;
    } else {
        this.x = -grid.tileWidth;
    }

    /* Check for collisions with the player, with slight
    manual adjustments (the +/- numbers) to make the game
    a little easier to play. */
    if (player.x < this.x - 20 + this.width &&
        player.x + player.width > this.x - 20 &&
        player.y < this.y + 10 + this.height &&
        player.height + player.y > this.y + 10) {
        player.reset('eaten');
    }

};



/*  Player Subclass -----

    The Player subclass is an instance of the Sprite
    superclass, taking in a position only.

    It has initial

*/

// Creates a new player from the Sprite
var Player = function(x, y) {

    Sprite.call(this, 'images/char-boy.png', x, grid.height, 48, 58);

    // Store the initial position for resetting the player
    this.initialX = x;
    this.initialY = y - grid.imageOffset;

    /* These are the 'new position' varaiables, used in
       animations when the player moves tile-to-tile */
    this.nx = x;
    this.ny = this.initialY;

    /* This flag ensures tells enables/disables movements
       to ensure no 'queue' of movements when animation */
    this.moving = false;

};

//  Ensure the Player has access to Sprite's prototypes
Player.prototype = Object.create(Sprite.prototype);

//  Sets the constructor of Player back to Player
Player.prototype.constructor = Player;

//  Updates the location of the Player
Player.prototype.update = function() {

    /* Animates movements along the x/y-axis if the new
        position is different than the current position.
        These positions are controlled by the handleInput
        function of the player. */
    // Left / -x
    if (this.x > this.nx) {
        this.x -= grid.tileWidth / 16;
    }
    // Right / +x
    if (this.x < this.nx) {
        this.x += grid.tileWidth / 16;
    }
    // Up / -y
    if (this.y > this.ny) {
        this.y -= grid.tileHeight / 16;
        // Reset if player falls into water
        if (this.y <= (grid.tileHeight / 4)) player.reset('drowning');
    }
    // Down / +y
    if (this.y < this.ny) {
        this.y += grid.tileHeight / 16;
    }

    /* If not currently moving, re-enable movements. This
       only occurs if the player is perfectly where they
       should be, forcing movements to be precise */
    if (this.x === this.nx && this.y === this.ny) {
        this.moving = false;
    }

    /* Checks for pick-ups (collisions with key). This
       function could have been on keyToCollect too, but
       player was chosen as this function pre-existed */
    if (keyToCollect.x < this.x + this.width &&
        keyToCollect.x + keyToCollect.width > this.x &&
        keyToCollect.y < this.y + this.height &&
        keyToCollect.height + keyToCollect.y > this.y) {

        // If all of those match, reset player with a 'win'
        player.reset('win');

    }

};

//  Manages the output from the Event Handler
Player.prototype.handleInput = function(key) {

    // If key is a gender change ('m' or 'f')
    if (key === 'f') {
        this.sprite = 'images/char-cat-girl.png';
    } else if (key === 'm') {
        this.sprite = 'images/char-boy.png';
    }

    // Otherwise, handle movements
    else {

        // Ensures the character isn't already moving
        if (!this.moving) {

            /* The following converts keypresses into
                movements, and stores those as nx/ny on the
                player for animating position changes. */

            // Left and right (x-axis) movements
            if (key === 'left' && this.x >= grid.tileWidth) {
                this.nx -= grid.tileWidth;
            }
            if (key === 'right' && this.x < (grid.width - grid.tileWidth)) {
                this.nx += grid.tileWidth;
            }

            // Up and down (y-axis) movements
            if (key === 'up' && this.y >= grid.tileHeight - grid.imageOffset) {
                this.ny -= grid.tileHeight;
            }
            if (key === 'down' && this.y < (grid.height - grid.tileHeight - grid.imageOffset)) {
                this.ny += grid.tileHeight;
            }

        }

        /* Sets moving to true, disabling more movements
           while player's position is animating. */
        this.moving = true;

    }
};

/*  Resets the player upon collision, drowning, or win,
    taking in a reason variable */
Player.prototype.reset = function(reason) {

    // Reset current position
    player.x = this.initialX;
    player.y = grid.height - grid.imageOffset;

    // Reset next position
    player.nx = this.x;
    player.ny = this.initialY;

    // If the reset was because of a 'win' (pickup)
    if (reason === 'win') {

        // Animate in the winningStar sprite
        winningStar.ny = grid.tileHeight * 3;

        // Animate out the winningStar sprite in 1.5s
        setTimeout(function(){winningStar.ny = grid.tileHeight * 7}, 1500);

        // Add to the games difficulty (enemy speeds)
        diffAdditive += 0.5;

    }

};



/*  Initialisations -----

    Creates and positions our player and the enemy bugs
    that block his path.

    Don't forget to add a .render() call for each of these
    in engine.js.

*/

//  Place keyToCollect to the collectible / game objective
var keyToCollect = new Collectible('images/Key.png', grid.tileWidth * 2,  grid.tileHeight - 16, 40, 40);

//  Setup 'winning star' as a hidden sprite
var winningStar = new Sprite('images/Star.png', grid.tileWidth * 2, grid.tileHeight * 7);

/*  Animates the location of the winningStar. This could
    have been a prototype on a new Star element, but that
    felt like adding another object to memory for no
    advantage (as we're only creating one star). */
winningStar.update = function() {

    // Animates winningStar up
    if (this.y > this.ny) {
        this.y -= grid.tileHeight / 16;
    }
    // Animates winningStar down
    if (this.y < this.ny) {
        this.y += grid.tileHeight / 16;
    }

};

//  Place a starting tile (just for visual effect)
var start = new Sprite('images/Selector.png', grid.tileWidth * 2,  grid.tileHeight * 5 - 16); // Adjusted y position to fit over tile

//  Create the player on the starting tile
var player = new Player(grid.tileWidth * 2, grid.tileHeight * 5);

/*  Initialise the difficulty additive, which increases
    with each win. */
var diffAdditive = 0;

/*  Enemies, one per line, two in the middle with a random
    start on the x-axis. These were all randomised, but
    that often produced undesirable results. Thus the
    decision was made to only randomise the y position. */
var randomX = function() {
    return Math.floor(Math.random() * 5) + 1;
};
var allEnemies = [
    new Enemy(grid.tileWidth * randomX(), grid.tileHeight, 1),
    new Enemy(grid.tileWidth * randomX(), grid.tileHeight * 2, 0.75),
    new Enemy(grid.tileWidth * randomX(), grid.tileHeight * 2, 1.5),
    new Enemy(grid.tileWidth * randomX(), grid.tileHeight * 3, 0.5)
];



/*  Pre-suppled Event Handler -----

    Listens for keyup events, and passes those alone as
    left, up, right, down, m, or f.

    This was supplied with the kit, only m/f was added.

*/
document.addEventListener('keyup', function(e) {
    var allowedKeys = {
        37: 'left',
        38: 'up',
        39: 'right',
        40: 'down',
        70: 'f',
        77: 'm'
    };

    player.handleInput(allowedKeys[e.keyCode]);
});
