/* Brody's Super Fun HTML5 Canvas Game */

The objective of this game is to move passed all of those
pesky bugs and get that key, othewise you'll never be able
to open the secret chest*.

Don't get bitten (or even touch those bugs, ew), and don't
fall into the water. 

Each win increases the speed of all of your opponents, so
don't win too much.

Don't like being a boy? Press 'f' to become a girl, and of 
course 'm' returns you to a boy. If only it was so simple
in real life...


*Secret chest is not included in Free-to-play edition of this game. Cost $3.49.